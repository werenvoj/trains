#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

struct train
{
    char Name;
    int ArriHour;
    int ArriMin;
    int DepHour;
    int DepMin;
    int dOneArrInMin;
    int dOneDepInMin;
    int dTwoArrInMin;
    int dTwoDepInMin;
};

int IsValidInput(int hour, char c, int minute, char over)
{
    if (c != ':' || hour < 0 || hour > 23 || minute < 0 || minute > 59 || !isspace(over))
    {
        printf("Nespravny vstup.\n");
        return 1;
    } 
    return 0;
}

void IsTransferable(struct train arrivalTrain, struct train transferableTrain1, struct train transferableTrain2)
{
    int howManyTransferable = 0;
    if ((arrivalTrain.dOneArrInMin+5 <= transferableTrain1.dOneDepInMin && arrivalTrain.dOneArrInMin+180 >= transferableTrain1.dOneArrInMin) || (arrivalTrain.dOneArrInMin+5 <= transferableTrain1.dTwoDepInMin && arrivalTrain.dOneArrInMin+180 >= transferableTrain1.dOneArrInMin && abs(transferableTrain1.dTwoDepInMin - arrivalTrain.dOneArrInMin) < abs(arrivalTrain.dOneArrInMin - transferableTrain1.dOneDepInMin)) || (arrivalTrain.dOneArrInMin+5 <= transferableTrain1.dTwoDepInMin && arrivalTrain.dOneArrInMin+180 >= transferableTrain1.dTwoArrInMin) || (arrivalTrain.dTwoArrInMin+5 <= transferableTrain1.dTwoDepInMin && arrivalTrain.dTwoArrInMin+180 >= transferableTrain1.dOneArrInMin && abs(transferableTrain1.dTwoDepInMin - arrivalTrain.dTwoArrInMin) > abs(arrivalTrain.dOneArrInMin - transferableTrain1.dOneArrInMin)))
        howManyTransferable++;
    if ((arrivalTrain.dOneArrInMin+5 <= transferableTrain2.dOneDepInMin && arrivalTrain.dOneArrInMin+180 >= transferableTrain2.dOneArrInMin) || (arrivalTrain.dOneArrInMin+5 <= transferableTrain2.dTwoDepInMin && arrivalTrain.dOneArrInMin+180 >= transferableTrain2.dOneArrInMin && abs(transferableTrain2.dTwoDepInMin - arrivalTrain.dOneArrInMin) < abs(arrivalTrain.dOneArrInMin - transferableTrain2.dOneDepInMin)) || (arrivalTrain.dOneArrInMin+5 <= transferableTrain2.dTwoDepInMin && arrivalTrain.dOneArrInMin+180 >= transferableTrain2.dTwoArrInMin) || (arrivalTrain.dTwoArrInMin+5 <= transferableTrain2.dTwoDepInMin && arrivalTrain.dTwoArrInMin+180 >= transferableTrain2.dOneArrInMin && abs(transferableTrain2.dTwoDepInMin - arrivalTrain.dTwoArrInMin) > abs(arrivalTrain.dOneArrInMin - transferableTrain2.dOneArrInMin)))
        howManyTransferable += 2;
    if (howManyTransferable == 3)
        printf("Z vlaku %c lze prestoupit na vlaky %c a %c.\n", arrivalTrain.Name, transferableTrain1.Name, transferableTrain2.Name);
    else if (howManyTransferable == 2)
        printf("Z vlaku %c lze prestoupit na vlak %c.\n", arrivalTrain.Name, transferableTrain2.Name);
    else if (howManyTransferable == 1)
        printf("Z vlaku %c lze prestoupit na vlak %c.\n", arrivalTrain.Name, transferableTrain1.Name);
    else
        printf("Z vlaku %c nelze prestupovat.\n", arrivalTrain.Name);
}

int main()
{
    printf("Cas prijezdu vlaku A:\n");
    struct train trainA;
    char ch;
    char over;
    trainA.Name = 'A';
    scanf("%d%c%d%c", &trainA.ArriHour, &ch, &trainA.ArriMin, &over);
    if (IsValidInput(trainA.ArriHour, ch, trainA.ArriMin, over) == 1)
        return 1;
    printf("Cas odjezdu vlaku A:\n");
    ch = ';';
    over  = ';';
    scanf("%d%c%d%c", &trainA.DepHour, &ch, &trainA.DepMin, &over);
    if (IsValidInput(trainA.DepHour, ch, trainA.DepMin, over) == 1)
        return 1;
    printf("Cas prijezdu vlaku B:\n");
    struct train trainB;
    ch = ';';
    over  = ';';
    trainB.Name = 'B';
    scanf("%d%c%d%c", &trainB.ArriHour, &ch, &trainB.ArriMin, &over);
    if (IsValidInput(trainB.ArriHour, ch, trainB.ArriMin, over) == 1)
        return 1;
    printf("Cas odjezdu vlaku B:\n");
    ch = ';';
    over  = ';';
    scanf("%d%c%d%c", &trainB.DepHour, &ch, &trainB.DepMin, &over);
    if (IsValidInput(trainB.DepHour, ch, trainB.DepMin, over) == 1)
        return 1;
    printf("Cas prijezdu vlaku C:\n");
    struct train trainC;
    ch = ';';
    over  = ';';
    trainC.Name = 'C';
    scanf("%d%c%d%c", &trainC.ArriHour, &ch, &trainC.ArriMin, &over);
    if (IsValidInput(trainC.ArriHour, ch, trainC.ArriMin, over) == 1)
        return 1;
    printf("Cas odjezdu vlaku C:\n");
    ch = ';';
    over  = ';';
    scanf("%d%c%d%c", &trainC.DepHour, &ch, &trainC.DepMin, &over);
    if (IsValidInput(trainC.DepHour, ch, trainC.DepMin, over) == 1)
        return 1;
    trainA.dOneArrInMin = trainA.ArriHour * 60 + trainA.ArriMin;
    trainA.dOneDepInMin = trainA.DepHour * 60 + trainA.DepMin;
    trainA.dTwoArrInMin = trainA.dOneArrInMin + 1440;
    trainA.dTwoDepInMin = trainA.dOneDepInMin + 1440;
    trainB.dOneArrInMin = trainB.ArriHour * 60 + trainB.ArriMin;
    trainB.dOneDepInMin = trainB.DepHour * 60 + trainB.DepMin;
    trainB.dTwoArrInMin = trainB.dOneArrInMin + 1440;
    trainB.dTwoDepInMin = trainB.dOneDepInMin + 1440;
    trainC.dOneArrInMin = trainC.ArriHour * 60 + trainC.ArriMin;
    trainC.dOneDepInMin = trainC.DepHour * 60 + trainC.DepMin;
    trainC.dTwoArrInMin = trainC.dOneArrInMin + 1440;
    trainC.dTwoDepInMin = trainC.dOneDepInMin + 1440;
    IsTransferable(trainA, trainB, trainC);
    IsTransferable(trainB, trainA, trainC);
    IsTransferable(trainC, trainA, trainB);
    return 0;
}