#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int value;
    struct node *next;
} node_t;

void nodeCreate(node_t **head)
{
    *head = nullptr;
}

void nodePushBack(node_t **head, int value)
{
    node_t *newH = (node_t *)malloc(sizeof(node_t));
    newH->value = value;
    newH->next = nullptr;
    if (*head == NULL)
        *head = newH;
    else
    {
        node_t *last = *head;
        while (last->next)
            last = last->next;

        last->next = newH;
    }
}

void nodePrint(const node_t *head)
{
    while (head)
    {
        printf("-> %d ", head->value);
        head = head->next;
    }
    printf("\n");
}

void nodeDelete(node_t *head)
{
    while (head != NULL)
    {
        node_t *next = head->next;
        free(head);
        head = next;
    }
}

int main()
{
    node_t *n;

    nodeCreate(&n);
    nodePushBack(&n, 1);
    nodePushBack(&n, 2);
    nodePushBack(&n, 3);
    nodePushBack(&n, 4);
    nodePrint(n);
    nodeDelete(n);
    return 0;
}
