#!/bin/bash

TEST_PATH=Tests

for f in {00..99} ; do
    if ! [ -e $TEST_PATH/00${f}_in.txt ]; then
        echo "All OK"
        exit 0
    fi

    ./a.out < $TEST_PATH/00${f}_in.txt > /tmp/out.txt
    if ! diff $TEST_PATH/00${f}_out.txt /tmp/out.txt ; then
        echo "Mismatch: $f"
        exit 1
    fi
done
echo "All OK"